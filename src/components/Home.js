import React from 'react';
// import ReactDOM from 'react-dom';

import PrimerComponenteJS from './PrimerComponente-JS';
import PrimerComponenteES6 from './PrimerComponente-ES6';

class Home extends React.Component{
    render(){

        return(
           <div>
                <PrimerComponenteJS   />
                <PrimerComponenteES6  />
           </div>
        ) 
        
    }
}

export default Home;
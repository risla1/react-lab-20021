import React from 'react';
import '../css/ES6.css';


class PrimerComponenteES6 extends React.Component{
    render(){
        return(
            <div className="divES6">
                <h2>Importando mi primer "Class Component" con ES6</h2>
            </div>
        ) 
    }
}

export default PrimerComponenteES6;
import React from 'react';
import Technologies from './Technologies';
import '../css/js.css';

class PrimerComponenteJS extends React.Component{
    
    constructor(props){
        super();
        this.state={
          tecnologias:[]  
        };
    }
    render(){

        const tecnologias = [
            {nombre: 'jquery', years:2019},
            {nombre: 'javascript', years:2020},
            {nombre: 'es6', years:2018},
            {nombre: 'vanillajs', years:2017}
        ]

        return (
            <div className="divJS">
                <h2>Importando mi primer "Componente  Funcional” conJS </h2>
                <Technologies techs={tecnologias} />
            </div>
        )
    }
}

export default PrimerComponenteJS;
import React from 'react';
import Technology from './Technology';

import '../css/ES6.css';


class Technologies extends React.Component{
    render(){
        return(
            <div>
                <h2>Technologies</h2>

               <ul>
                    { Object.keys(this.props.techs).map( key=>(
                        <Technology 
                            key={key}
                            tech={this.props.techs[key]}
                        />
                    )) }
                     <li>Estatico</li>
               </ul>


            </div>
             
        ) 
    }
}

export default Technologies;
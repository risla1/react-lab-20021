import React from 'react';
import '../css/ES6.css';


class Technology extends React.Component{
    render(){

        const {nombre, years} = this.props.tech;

        return(
           
            <li>
                {nombre}:{years}
            </li>
           
             
        ) 
    }
}

export default Technology;